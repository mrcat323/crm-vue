import { userInfo } from '@/rest/api';
import { createStore } from 'vuex';
import createdPersistentState from 'vuex-persistedstate';

export default createStore({
    state: {
        user: {},
        isAuth: false
    },
    getters: {},
    mutations: {
        changeAuthState(state, payload) {
            state.isAuth = payload;
        },
        changeUserState(state, payload) {
            state.user = payload;
        }
    },
    actions: {
        getUserData(context) {
            userInfo()
                .then(res => {
                    context.commit('changeUserState', res)
                })
                .catch(err => console.log(err))
        }
    },
    plugins: [createdPersistentState()]
});