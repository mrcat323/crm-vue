import { createRouter, createWebHistory } from 'vue-router'
import LoginView from '../views/LoginView.vue'
import RequestsView from '../views/RequestsView.vue'
import PageNotFound from '../views/PageNotFoundView.vue'
import CreateRequest from '../views/Requests/CreateRequest'
import EditRequest from '../views/Requests/EditRequest'
import StatusHistory from '../views/StatusHistoryView';


const routes = [
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue'),
    meta: {
      auth: true
    }
  },
  {
    path: '/login',
    name: 'login',
    component: LoginView,
    meta: {
      auth: false
    }
  },
  {
    path: '/',
    name: 'requests',
    component: RequestsView,
    meta: {
      auth: true
    }
  },
  {
    path: '/requests/create',
    name: 'create-request',
    component: CreateRequest,
    meta: {
      auth: true
    }
  },
  {
    path: '/requests/edit/:id',
    name: 'edit-request',
    component: EditRequest,
    meta: {
      auth: true
    }
  },
  {
    path: '/history',
    name: 'history',
    component: StatusHistory,
    meta: {
      auth: true
    }
  },
  {
    path: '/:pathMatch(.*)*',
    name: '404',
    component: PageNotFound
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  if (to.meta.auth && to.name != 'login' && !localStorage.getItem('token')) next({ name: 'login' });
  else if (localStorage.getItem('token') && to.name == 'login') next({ name: from.name });
  else next();
})

export default router
