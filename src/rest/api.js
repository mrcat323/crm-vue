import { apiUrl } from "@/configs/config";
import { getToken } from "@/configs/helpers";

export const login = async (params) => {
    try {
        const res = await fetch(`${apiUrl}/login`, {
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify(params)
        });

        return await res.json();
    } catch (error) {
        console.log(error)
    }
};

export const userInfo = async () => {
    const res = await fetch(`${apiUrl}/user`, {
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`
        }
    });
    if (res.status == 401) {
        localStorage.removeItem('token');
    }

    return await res.json();
}

export const logout = async () => {
    const res = await fetch(`${apiUrl}/logout`, {
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`
        },
        method: 'POST',
    });
    if (res.status == 401) {
        localStorage.removeItem('token');
    }

    return await res.json();
}

export const getAllRequests = async () => {
    try {
        const res = await fetch(`${apiUrl}/requests`, {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${getToken()}`
            }
        });

        return await res.json();
    } catch(error) {
        console.log(error);
    }
}

export const getOneRequest = async (id) => {
    try {
        const res = await fetch(`${apiUrl}/request/${id}`, {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${getToken()}`
            }
        });

        return await res.json();
    } catch(error) {
        console.log(error);
    }
}

export const createRequest = async (params) => {
    try {
        const res = await fetch(`${apiUrl}/requests/store`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${getToken()}`
            },
            body: JSON.stringify(params)
        });

        return await res.json();
    } catch(error) {
        console.log(error);
    }
}

export const updateRequest = async (params) => {
    try {
        const res = await fetch(`${apiUrl}/requests/update`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${getToken()}`
            },
            body: JSON.stringify(params)
        });

        return await res.json();
    } catch(error) {
        console.log(error);
    }
}

export const getAllOperators = async () => {
    const res = await fetch(`${apiUrl}/operators`, {
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`
        }
    });

    if (res.status == 401) {
        localStorage.removeItem('token');
    }

    return await res.json();
}

export const searchRequests = async (params) => {
    const queryParams = new URLSearchParams(params).toString();
    const res = await fetch(`${apiUrl}/requests/search?${queryParams}`, {
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`
        }
    });

    if (res.status == 401) {
        localStorage.removeItem('token');
    }

    return await res.json();
}

export const getAllHistory = async () => {
    const res = await fetch(`${apiUrl}/history`, {
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`
        }
    });

    if (res.status == 401) {
        localStorage.removeItem('token');
    }

    return await res.json();
}